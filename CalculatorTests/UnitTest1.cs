using System;
using Xunit;
using CalculatorBL;

namespace XUnitTestsCalc
{
    public class UnitTest1
    {
        [Fact]
        public void ReturnZeroIfInputEmpty()
        {
            var calculator = new StringCalculator();

            var sum = calculator.Add("");

            Assert.Equal(sum, 0);
        }

        [Fact]
        public void ReturnNumberIfInputIsAnyNumber()
        {
            var calculator = new StringCalculator();

            var sum = calculator.Add("7");

            Assert.Equal(sum, 7);
        }

        [Fact]
        public void ReturnSumIfInputIsTwoNumbers()
        {
            var calculator = new StringCalculator();

            var sum = calculator.Add("1089/*/6/n4");

            Assert.Equal(sum, 10);
        }



    }
}

