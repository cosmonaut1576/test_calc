﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CalculatorBL
{

    public class StringCalculator
    {
        public int Add(string numbers)
        {
            if (String.IsNullOrEmpty(numbers)) return 0;
            int sum = 0;
            int number;
            Regex regexF = new Regex(@"-[0-9]+");
            if (regexF.IsMatch(numbers))
            {
                Console.WriteLine("Negatives are not allowed:");
                foreach (Match match in regexF.Matches(numbers))
                {
                    Console.Write(match.Value + "/");
                }
                return 0;
            }
            else
            {
                Regex regexT = new Regex(@"[0-9]+");
                Match match = regexT.Match(numbers); // Получаем совпадения в экземпляре класса Match         
                while (match.Success)               // отображаем все совпадения
                {
                    number = Convert.ToInt32(match.Value);
                    if (number > 1000) { match = match.NextMatch(); continue; }
                    sum += number;
                    match = match.NextMatch();      // Переходим к следующему совпадению
                }
                return sum;
            }
        }

    }
    
}


